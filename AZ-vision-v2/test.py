import requests

from api.services.face_service import training_face
from model.camera_model import CameraModel


def test_faceload():
    url = 'http://171.244.49.1/vms-server/face-upload'
    file = {'file': open(r'C:\Users\dell\Pictures\Saved Pictures\crop.jpg', 'rb')}
    data = {'faceId': 2,
            'eventId': 2,
            'deviceId': 3,
            'recordTime': '2020:09:08 10:06:05'}

    x = requests.post(url, files=file, data=data)
    print(x)


def test_download_image():
    url = 'http://171.244.49.1/vms-server/image/download/'
    id = 2
    r = requests.get(f"{url}{id}", stream=True)
    if r.status_code == 200:
        with open('test.jpg', 'wb') as f:
            for chunk in r:
                f.write(chunk)


def test_camera():
    datas = [{"DeviceId": 1,
              "CameraLink": 0,
              "TypeRecognition": "[1]"},
             {"DeviceId": 2,
              "CameraLink": 'rtsp://admin:4321@192.168.1.96/profile1/media.smp',
              "TypeRecognition": "[1]"}
             ]
    for data in datas:
        CameraModel(data)
    while True:
        pass


if __name__ == '__main__':
    training_face()
    test_camera()
