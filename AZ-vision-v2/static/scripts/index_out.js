HOST = '192.168.1.130'
POST = '5000'
function ajax_helper(url,type,data, success, error){
    var onSuccess = function () { };
    var onError = function () { };

    if (typeof (success) === 'function') {
                onSuccess = success;
            }
    if (typeof (error) === 'function') {
                onError = error;
            }

    return $.ajax({
        url: url,
        data: data ? JSON.stringify(data):"",
        type: type,
        dataType: "json",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (response) {
            onSuccess(response)
        },
        error: function (error) {
            onError(error)
        },
    });
}

//var mapCategory = function (dataFromServer, idName, textName) {
//    if (idName && textName) {
//        return ko.utils.arrayMap(dataFromServer, function (item) {
//            return new Category(item[idName], item[textName]);
//        });
//    } else {
//        return ko.utils.arrayMap(dataFromServer, function (item) {
//            return new Category(item.id, item.name);
//        });
//    }
//};
//var Category = function (id, name) {
//    this.id = id;
//    this.name = name;
//};

// Here's my data model
function ViewModel() {
    self = this
    self.devices = ko.observableArray();
    self.device_id = ko.observable()
    self.src_video = ko.computed(function() {
        return ''.concat('http://',HOST,':',POST,'/api/stream/in/video_feed/',self.device_id());
    }, this);
    self.reload_video = function(){
        $('#video_stream').load('#video_stream')
    }
	ajax_helper(
            url = ''.concat('http://',HOST,':',POST,'/api/stream/get_all_camera'),
            type = 'GET',
            data = '',
            success = function (d) {
                if (d) {
                    for (i = 0; i < d.data.length; i++) {
                        self.devices.push(d.data[i])
                    }
                    console.log(self.devices())
                }
            },
        );
};

// A $( document ).ready() block.
$( document ).ready(function() {
    viewModel = ViewModel()
    ko.applyBindings(viewModel)
});
