from flask import Flask, redirect, url_for

from api.api_ai import ai
from api.api_config import config
from api.api_stream_video import stream
from api.controler.config_controller import get_config_device
from api.services.camera_service import start_camera
from api.services.face_service import training_face

flask_app = Flask(__name__)


def create_app():
    flask_app.register_blueprint(ai, url_prefix="/api/ai")
    flask_app.register_blueprint(config, url_prefix="/api/config")
    flask_app.register_blueprint(stream, url_prefix="/api/stream")
    return flask_app


def set_up_box_weak_up():
    training_face()
    camera_data = get_config_device()
    if camera_data:

        start_camera(camera_data)


@flask_app.route('/')
def index():
    return redirect(url_for('stream.index_in'))


if __name__ == '__main__':
    set_up_box_weak_up()
    app = create_app()
    app.run(host='192.168.1.130', threaded=True)
