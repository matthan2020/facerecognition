import json
import os
import time
from collections import deque
from datetime import datetime
from threading import Thread

import cv2
import numpy as np
from PIL import ImageDraw, Image

from api.services.face_recognition_service import recognition_face, send_face_rec
from api.services.face_service import add_face, get_name_by_face_id
from model.face_recognition_model import FaceRecognitionModel, save_known_faces

face_recognition_model = FaceRecognitionModel.get_instance()

track_faces = deque(maxlen=100)
track_times = deque(maxlen=100)

path = os.path.dirname(__file__)
face_cascade = cv2.CascadeClassifier(os.path.join(path, "haarcascade_frontalface_alt2.xml"))
ds_factor = 0.6


def recog_face(frame, device_id):
    """
    recognition face in frame
    :param device_id: id of device
    :param frame: frame of video stream
    :return: frame
    """
    have_new_face = False
    frame, face_ids, track_time, face_encodings, face_locations = recognition_face(frame)
    if face_ids:
        for i in range(len(face_ids)):
            send_face_enable = False
            if face_ids[i] in track_faces:
                index = track_faces.index(face_ids[i])
                if (timestamp(track_time) - timestamp(track_times[index])) > 10:
                    send_face_enable = True

                    # delete old record
                    del track_faces[index]
                    del track_times[index]
            else:
                send_face_enable = True

            if send_face_enable:
                # insert new record
                track_faces.append(face_ids[i])
                track_times.append(track_time)

                # send envet track face
                id_return = send_face_rec(face_ids[i], device_id, track_time, frame)
                if face_ids[i] is None and id_return:
                    have_new_face = True
                    obj = {'im_id': '',
                           'face_id': id_return,
                           'image': face_encodings[i]}
                    add_face(obj)
    if have_new_face:
        save_known_faces(face_recognition_model.known_face_encodings, face_recognition_model.known_face_id,
                         face_recognition_model.known_image_id)

    if face_locations:
        name = get_name_by_face_id(face_ids[0])
        has_name = False
        # for i in range(len(face_recognition_model.name_tracking)):
        #     if face_recognition_model.name_tracking[i]['device_id'] == device_id:
        #         face_recognition_model.name_tracking[i]['name'] = name
        #         has_name = True
        #         break

        if not has_name:
            face_recognition_model.name_tracking.append({'name': name,
                                                         'device_id': device_id})

        top, right, bottom, left = face_locations[0]
        face = frame[top * 2:bottom * 2, left * 2:right * 2]
        cv2.imwrite(f'{device_id}.jpg', face)


def spin(seconds):
    """Pause for set amount of seconds, replaces time.sleep so program doesnt stall"""

    time.sleep(seconds)


def timestamp(dt):
    """covert date time to seconds"""
    return (dt - datetime(1970, 1, 1)).total_seconds()


class CameraModel:
    """Independent camera feed
    Uses threading to grab IP camera frames in the background

    @param width - Width of the video frame
    @param height - Height of the video frame
    @param stream_link - IP/RTSP/Webcam link
    @param aspect_ratio - Whether to maintain frame aspect ratio or force into fraame
    """

    def __init__(self, data, deque_size=1):
        # Flag send link to server
        self.send_link_to_server = False

        # Initialize deque used to store frames read from the stream
        self.deque = deque(maxlen=deque_size)

        # Slight offset is needed since PyQt layouts have a built in padding
        # So add offset to counter the padding
        self.offset = 16

        self.device_id = data['DeviceId']
        self.camera_stream_link = data['CameraLink']
        self.type_recognition = json.loads(data['TypeRecognition'])
        self.face_recognition_model = FaceRecognitionModel.get_instance()

        # Flag to check if camera is valid/working
        self.online = False
        self.capture = None

        self.load_network_stream()

        # stop thread
        self.run_thread = True

        # Start background frame grabbing
        self.get_frame_thread = Thread(target=self.get_frame, args=())
        self.get_frame_thread.daemon = True
        self.get_frame_thread.start()

        if 1 in self.type_recognition:
            # Start face recognition
            self.face_recognition_thread = Thread(target=self.face_recognition, args=())
            self.face_recognition_thread.daemon = True
            self.face_recognition_thread.start()

        print('Started camera: {}'.format(self.camera_stream_link))
        print(self.device_id)

    def load_network_stream(self):
        """Verifies stream link and open new stream if valid"""

        def verify_network_stream(link):
            """Attempts to receive a frame from given link"""
            cap = cv2.VideoCapture(link)
            if not cap.isOpened():
                return False
            cap.release()
            return True

        def load_network_stream_thread():
            if verify_network_stream(self.camera_stream_link):
                self.capture = cv2.VideoCapture(self.camera_stream_link)
                self.online = True

        self.load_stream_thread = Thread(target=load_network_stream_thread, args=())
        self.load_stream_thread.daemon = True
        self.load_stream_thread.start()

    def get_frame(self):
        """Reads frame, resizes, and converts image to pixmap"""
        while self.run_thread:
            try:
                if self.capture.isOpened() and self.online:
                    # Read next frame from stream and insert into deque
                    status, frame = self.capture.read()
                    if status:
                        self.deque.append(frame)
                    else:
                        self.capture.release()
                        self.online = False
                else:
                    # Attempt to reconnect
                    print('attempting to reconnect', self.camera_stream_link)
                    self.load_network_stream()
                    spin(5)
                spin(.001)
            except AttributeError:
                pass

    def get_video_stream(self):
        if self.deque:
            frame = self.deque[0]
            frame = cv2.resize(frame, None, fx=ds_factor, fy=ds_factor, interpolation=cv2.INTER_AREA)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            face_rects = face_cascade.detectMultiScale(gray, 1.3, 5)
            for (x, y, w, h) in face_rects:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                break

            # get face tracking
            face = cv2.imread(f'{self.device_id}.jpg')
            if face is not None:
                face = cv2.resize(face, (60, 60))

                # get size of frame
                height, width, channels = frame.shape
                # put face to frame
                frame[0:60, width - 60:width] = face

                # put name to frame
                name = 'Unknown'
                for data in face_recognition_model.name_tracking:
                    if data['device_id'] == self.device_id:
                        name = data['name']

                img_pil = Image.fromarray(frame)
                draw = ImageDraw.Draw(img_pil)
                draw.text((width - 100, 70), name, fill=(255, 255, 255))
                frame = np.array(img_pil)

                # cv2.putText(frame, name, (width - 100, 70), font, 0.3, (255, 255, 255), 1)

            frame = cv2.resize(frame, (1280, 720))
            ret, jpeg = cv2.imencode('.jpg', frame)
            return jpeg.tobytes()

    def face_recognition(self):
        while self.run_thread:
            if self.deque:
                frame = self.deque[0]
                recog_face(frame, self.device_id)
                spin(0.7)

    def stop_all_thread(self):
        self.run_thread = False
